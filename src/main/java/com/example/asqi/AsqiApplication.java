package com.example.asqi;

import com.example.config.AppConfig;
import com.example.dao.FootBallMatchesDAO;
import com.example.service.Runner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class AsqiApplication {

	public static void main(String[] args) {
		System.out.println("Hello ASQI");
		for (int i = 0; i < 10; i++) {
			System.out.println();
		}
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		FootBallMatchesDAO footBallMatchesDAO = context.getBean(FootBallMatchesDAO.class);
		Runner runner = new Runner();
		runner.runMain(footBallMatchesDAO);
		for (int i = 0; i < 10; i++) {
			System.out.println();
		}
		System.out.println("Finished");
	}


}
