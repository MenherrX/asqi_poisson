package com.example.dao;

import com.example.entity.HistoricalMatches;
import com.example.entity.UpcomingMatches;
import com.example.service.MatchesRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class FootBallMatchesDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public FootBallMatchesDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final String insertSql =
            String.format("INSERT INTO output ( matchDateTime,  teamOne,  teamTwo,  teamOneOdds,  teamTwoOdds,  drawOdds,  uploadDateTime) VALUES (?,?,?,?,?,?,?)");

    public List<HistoricalMatches> findAll() throws SQLException {
        return jdbcTemplate.query("" +
                "select " +
                "HomeTeam as HomeTeam," +
                "AwayTeam as AwayTeam," +
                "FTHG as FullTimeHomeGoal," +
                "FTAG as FullTimeAwayGoal," +
                "Date," +
                "Time " +
                "from premier_league", new MatchesRowMapper());
    }

    public void saveRecord(List<UpcomingMatches> upcomingMatches) {
        // define query arguments
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//        Object[] params = new Object[]{upcomingMatches.getMatchDateTime().format(formatter), upcomingMatches.getTeamOne(),upcomingMatches.getTeamTwo(),upcomingMatches.getWinOddsTeamOne(),upcomingMatches.getWinOddsTeamTwo(),upcomingMatches.getDrawOdds(),LocalDateTime.now().format(formatter)};

        // define SQL types of the arguments
//        int[] types = new int[]{Types.TIMESTAMP, Types.VARCHAR, Types.VARCHAR, Types.DOUBLE, Types.DOUBLE, Types.DOUBLE, Types.TIMESTAMP};

        // execute insert query to insert the data
        // return number of row / rows processed by the executed query
        int[] row = jdbcTemplate.batchUpdate(insertSql, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i)
                    throws SQLException {

                UpcomingMatches upcomingMatches1 = upcomingMatches.get(i);
                ps.setDate(1, Date.valueOf(upcomingMatches1.getMatchDateTime().toLocalDate()));
                ps.setString(2, upcomingMatches1.getTeamOne());
                ps.setString(3, upcomingMatches1.getTeamTwo());
                ps.setDouble(4, upcomingMatches1.getWinOddsTeamOne());
                ps.setDouble(5, upcomingMatches1.getWinOddsTeamTwo());
                ps.setDouble(6, upcomingMatches1.getDrawOdds());
                ps.setDate(7, Date.valueOf(LocalDateTime.now().toLocalDate()));

            }

            @Override
            public int getBatchSize() {
                return upcomingMatches.size();
            }
        });
        StringBuilder stringBuilder = new StringBuilder();
        for (int a : row) {
            stringBuilder.append(a);
        }
        ;
        System.out.println("Rows Inserted Parity " + stringBuilder);

    }


}

