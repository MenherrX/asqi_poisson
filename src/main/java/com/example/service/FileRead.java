package com.example.service;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Service
public class FileRead {
    public static final String fileName = "upcomingMatches.txt";

    public BufferedReader readFile() {
        InputStreamReader input = null;
        input = new InputStreamReader(
                this.getClass().getResourceAsStream("/" + fileName));
        BufferedReader bufRead = new BufferedReader(input);
        return bufRead;
    }

}
