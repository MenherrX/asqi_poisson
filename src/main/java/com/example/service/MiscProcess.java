package com.example.service;

import com.example.entity.UpcomingMatches;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class MiscProcess {

    public static int getPoisson(double lambda) {
        double L = Math.exp(-lambda);
        double p = 1.0;
        int k = 0;

        do {
            k++;
            p *= Math.random();
        } while (p > L);

        return k - 1;
    }

    public List<UpcomingMatches> parseFiletoUpcoming(BufferedReader br) {
        List<UpcomingMatches> upcomingMatchesList = new ArrayList<>();
        try {
            while (true) {
                String readLine = br.readLine();
                if (readLine != null) {
                    UpcomingMatches upcomingMatches = new UpcomingMatches();
                    String[] array1 = readLine.split(",");
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm");
                    upcomingMatches.setMatchDateTime(LocalDateTime.parse(array1[0], formatter));
                    String[] array2 = array1[1].split(":v:");
                    upcomingMatches.setTeamOne(array2[0].trim());
                    upcomingMatches.setTeamTwo(array2[1].trim());
                    upcomingMatchesList.add(upcomingMatches);
                } else
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return upcomingMatchesList;
    }
}
