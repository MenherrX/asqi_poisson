package com.example.service;

import com.example.entity.HistoricalMatches;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class MatchesRowMapper implements RowMapper<HistoricalMatches> {
    @Override
    public HistoricalMatches mapRow(ResultSet resultSet, int i) throws SQLException {
        LocalDateTime localDateTime = LocalDateTime.of(
                resultSet.getDate("Date").toLocalDate(),
                resultSet.getTime("Time").toLocalTime());
        HistoricalMatches historicalMatches = new HistoricalMatches(resultSet.getString("HomeTeam"),
                resultSet.getString("AwayTeam"),
                resultSet.getInt("FullTimeHomeGoal"),
                resultSet.getInt("FullTimeAwayGoal"),
                localDateTime
        );
        return historicalMatches;
    }
}
