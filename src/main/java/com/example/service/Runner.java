package com.example.service;

import com.example.dao.FootBallMatchesDAO;
import com.example.entity.HistoricalMatches;
import com.example.entity.TeamInfo;
import com.example.entity.UpcomingMatches;
import org.springframework.beans.factory.annotation.Configurable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configurable
public class Runner {
    List<HistoricalMatches> historicalMatchesList;
    List<UpcomingMatches> upcomingMatchesList;

    private MiscProcess miscProcess = new MiscProcess();

    private FileRead fileRead = new FileRead();

    public Runner() {
    }

    public void runMain(FootBallMatchesDAO footBallMatchesDAO) {
        loadAllData(footBallMatchesDAO);
        List<String> teamLists = prepareListOfTeams(upcomingMatchesList);
        List<TeamInfo> teamInfos = prepareTeamInfos(teamLists);
        calculateOdds(teamInfos);
        persistOutput(upcomingMatchesList, footBallMatchesDAO);

    }

    private void persistOutput(List<UpcomingMatches> upcomingMatchesList, FootBallMatchesDAO footBallMatchesDAO) {
        footBallMatchesDAO.saveRecord(upcomingMatchesList);

    }

    private void calculateOdds(List<TeamInfo> teamInfos) {
        for (UpcomingMatches upcomingMatch : upcomingMatchesList
        ) {
            Double team1win = 0D, team2win = 0D, draw = 0D;
            TeamInfo team1 = teamInfos.stream().filter(ele -> ele.getTeamName().equals(upcomingMatch.getTeamOne())).collect(Collectors.toList()).get(0);
            TeamInfo team2 = teamInfos.stream().filter(ele -> ele.getTeamName().equals(upcomingMatch.getTeamTwo())).collect(Collectors.toList()).get(0);
            for (int j = 0; j < 10000; j++) {
                if (team1.getPoissonGenerated().get(j) > team2.getPoissonGenerated().get(j))
                    team1win++;
                else if (team1.getPoissonGenerated().get(j) < team2.getPoissonGenerated().get(j))
                    team2win++;
                else
                    draw++;
            }
            team1win = team1win / 10000;
            team2win = team2win / 10000;
            draw = draw / 10000;
            upcomingMatch.setWinOddsTeamOne(1 / team1win);
            upcomingMatch.setWinOddsTeamTwo(1 / team2win);
            upcomingMatch.setDrawOdds(1 / draw);
        }
    }

    private List<TeamInfo> prepareTeamInfos(List<String> teamLists) {
        List<TeamInfo> teamInfos = new ArrayList<>();
        for (String teamName : teamLists
        ) {
            TeamInfo teamInfo = new TeamInfo();
            teamInfo.setTeamName(teamName);
            computeAverageForTeam(teamInfo);
            computePoissonForTeam(teamInfo);
            teamInfos.add(teamInfo);
        }
        return teamInfos;
    }

    private void computePoissonForTeam(TeamInfo teamInfo) {
        List<Integer> poissonDistribution = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            poissonDistribution.add(miscProcess.getPoisson(teamInfo.getAverageGoals()));
        }
        teamInfo.setPoissonGenerated(poissonDistribution);
    }

    private void computeAverageForTeam(TeamInfo team) {
        List<HistoricalMatches> historicalMatches = historicalMatchesList
                .stream()
                .filter(ele -> (ele.getHomeTeam().equals(team.getTeamName()) || ele.getAwayTeam().equals(team.getTeamName())))
                .sorted(Comparator.comparing(HistoricalMatches::getMatchData).reversed()).collect(Collectors.toList());
        team.setAverageGoals(computeAverage(team.getTeamName(), historicalMatches));
    }

    private Double computeAverage(String teamName, List<HistoricalMatches> historicalMatches) {
        Double sum = 0D;
        historicalMatches = historicalMatches.subList(0, 8);
        for (HistoricalMatches historicalMatch : historicalMatches
        ) {
            if (historicalMatch.getHomeTeam().equals(teamName)) {
                sum += historicalMatch.getFullTimeHomeGoals();
            } else
                sum += historicalMatch.getFullTimeAwayGoals();
        }
        return sum / historicalMatches.size();
    }

    private List<String> prepareListOfTeams(List<UpcomingMatches> upcomingMatchesList) {
        return upcomingMatchesList
                .stream()
                .flatMap(ele -> Stream.of(ele.getTeamOne(), ele.getTeamTwo()))
                .collect(Collectors.toList());

    }

    private void loadAllData(FootBallMatchesDAO footBallMatchesDAO) {
        try {
            historicalMatchesList = loadHistoricalData(footBallMatchesDAO);
            upcomingMatchesList = miscProcess.parseFiletoUpcoming(fileRead.readFile());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private List<HistoricalMatches> loadHistoricalData(FootBallMatchesDAO footBallMatchesDAO) throws SQLException {
        return footBallMatchesDAO.findAll();
    }


}
