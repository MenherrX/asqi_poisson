package com.example.entity;

import java.util.List;

public class TeamInfo {
    String teamName;
    Double averageGoals;
    List<Integer> poissonGenerated;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Double getAverageGoals() {
        return averageGoals;
    }

    public void setAverageGoals(Double averageGoals) {
        this.averageGoals = averageGoals;
    }

    public List<Integer> getPoissonGenerated() {
        return poissonGenerated;
    }

    public void setPoissonGenerated(List<Integer> poissonGenerated) {
        this.poissonGenerated = poissonGenerated;
    }
}
