package com.example.entity;

import java.time.LocalDateTime;

public class HistoricalMatches {
    String homeTeam;
    String awayTeam;
    Integer fullTimeHomeGoals;
    Integer fullTimeAwayGoals;
    LocalDateTime matchData;

    public HistoricalMatches(String homeTeam, String awayTeam, Integer fullTimeHomeGoals, Integer fullTimeAwayGoals, LocalDateTime matchData) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.fullTimeHomeGoals = fullTimeHomeGoals;
        this.fullTimeAwayGoals = fullTimeAwayGoals;
        this.matchData = matchData;
    }

    public HistoricalMatches() {

    }


    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Integer getFullTimeHomeGoals() {
        return fullTimeHomeGoals;
    }

    public void setFullTimeHomeGoals(Integer fullTimeHomeGoals) {
        this.fullTimeHomeGoals = fullTimeHomeGoals;
    }

    public Integer getFullTimeAwayGoals() {
        return fullTimeAwayGoals;
    }

    public void setFullTimeAwayGoals(Integer fullTimeAwayGoals) {
        this.fullTimeAwayGoals = fullTimeAwayGoals;
    }


    public LocalDateTime getMatchData() {
        return matchData;
    }

    public void setMatchData(LocalDateTime matchData) {
        this.matchData = matchData;
    }
}
