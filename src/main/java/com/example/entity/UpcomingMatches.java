package com.example.entity;

import java.time.LocalDateTime;

public class UpcomingMatches {
    String TeamOne;
    String TeamTwo;
    Double winOddsTeamOne;
    Double winOddsTeamTwo;
    Double drawOdds;
    LocalDateTime matchDateTime;

    public String getTeamOne() {
        return TeamOne;
    }

    public void setTeamOne(String teamOne) {
        this.TeamOne = teamOne;
    }

    public String getTeamTwo() {
        return TeamTwo;
    }

    public void setTeamTwo(String teamTwo) {
        this.TeamTwo = teamTwo;
    }

    public Double getWinOddsTeamOne() {
        return winOddsTeamOne;
    }

    public void setWinOddsTeamOne(Double winOddsTeamOne) {
        this.winOddsTeamOne = winOddsTeamOne;
    }

    public Double getWinOddsTeamTwo() {
        return winOddsTeamTwo;
    }

    public void setWinOddsTeamTwo(Double winOddsTeamTwo) {
        this.winOddsTeamTwo = winOddsTeamTwo;
    }

    public Double getDrawOdds() {
        return drawOdds;
    }

    public void setDrawOdds(Double drawOdds) {
        this.drawOdds = drawOdds;
    }

    public LocalDateTime getMatchDateTime() {
        return matchDateTime;
    }

    public void setMatchDateTime(LocalDateTime matchDateTime) {
        this.matchDateTime = matchDateTime;
    }
}
